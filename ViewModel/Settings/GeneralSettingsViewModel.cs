﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel.Settings
{
    public class GeneralSettingsViewModel
    {
        [Display(Name = "نام شرکت")]
        public string CompanyName { get; set; }

        [Display(Name = "کد اقتصادی")]
        public string EconomicId { get; set; }

        [Display(Name = "کد پستی")]
        public string PostId { get; set; }

        [Display(Name = "آدرس")]
        public string Address { get; set; }

        [Display(Name = "تلفن")]
        public string Tell { get; set; }
    }
}