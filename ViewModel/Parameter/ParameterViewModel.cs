﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel.Parameter
{
    public class ParameterViewModel
    {
        public Guid Id { get; set; }

        [Display(Name = "نام پارامتر")]
        public string ParameterName { get; set; }

        [Display(Name = "شماره پارامتر")]
        public string ParameterNumber { get; set; }

        [Display(Name = "زمان استاندارد")]
        public int StandardTime { get; set; }

        [Display(Name = "زمان هدر رفته")]
        public int WastedTime { get; set; }

        [Display(Name = "جامعه آماری")]
        public int StandardCount { get; set; }
    }
}