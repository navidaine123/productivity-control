﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel.DailyInformation
{
    public class ProccesResultDto
    {
        public int DiffrenceDoTime { get; set; }

        public int DiffrenceQuantity { get; set; }
    }
}
