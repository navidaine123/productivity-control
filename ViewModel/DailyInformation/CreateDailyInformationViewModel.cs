﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel.DailyInformation
{
    public class CreateDailyInformationViewModel
    {
        public string PersonNumber { get; set; }

        public string ParameterNumber { get; set; }

        public DateTime Date { get; set; }

        public int DoTime { get; set; }

        public int Quantity { get; set; }

        public int DiffrenceDoTime { get; set; }

        public int DiffrenceQuantity { get; set; }
    }
}
