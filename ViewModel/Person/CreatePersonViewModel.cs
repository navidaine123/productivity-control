﻿using Model.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace ViewModel.Person
{
    public class CreatePersonViewModel
    {
        [Display(Name = "نام")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [Display(Name = "حقوق پایه")]
        public long BaseSalary { get; set; }

        public DeployType DeployType { get; set; }

        public string Position { get; set; }

        public DateTime DeployDate { get; set; }
    }
}