﻿using Model;
using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ViewModel.DailyInformation;

namespace Service
{
    public class WorkSheetService : IWorkSheetService
    {
        private readonly IParameterRepository _parameterRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IWorkSheetRepository _workSheetRepository;

        private readonly IUnitOfWork _unitOfWork;

        public WorkSheetService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _parameterRepository = unitOfWork.ParameterRepository;
            _personRepository = unitOfWork.PersonRepository;
            _workSheetRepository = unitOfWork.WorkSheetRepository;
        }

        public async Task<Model.WorkSheet> CreateWorkSheetAsync(CreateDailyInformationViewModel viewModel)
        {
            var paramId =
                (await _parameterRepository.GetByParameterNumber(viewModel.ParameterNumber))
                .Id;
            var personId =
                (await _personRepository.GetPersonByNumber(viewModel.PersonNumber))
                .Id;
            var workSheet = new WorkSheet()
            {
                Date = viewModel.Date,
                ParameterId = paramId,
                PersonId = personId,
                DoTime = viewModel.DoTime,
                Quantity = viewModel.Quantity,
                DiffrenceQuantity = viewModel.DiffrenceQuantity,
                DiffrenceDoTime = viewModel.DiffrenceDoTime
            };

            var res = await _workSheetRepository.AddAsync(workSheet);

            await _unitOfWork.SaveAsync();

            return res;
        }

        public async Task<ProccesResultDto> proccesDailyInfo(int doTime, int quantity, int paramNumber, int personNumber)
        {
            var parameter = await _parameterRepository
                .GetByParameterNumber(paramNumber.ToString());
            var person = await _personRepository.GetPersonByNumber(personNumber.ToString());

            if (parameter != null)
            {
                var res = new ProccesResultDto
                {
                    DiffrenceDoTime = (parameter.StandardTime * quantity) - doTime,
                };
                res.DiffrenceQuantity = res.DiffrenceDoTime * person.BaseMinuteSalary;
                return res;
            }
            return null;
        }
    }
}