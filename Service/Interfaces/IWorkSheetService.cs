﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ViewModel.DailyInformation;

namespace Service.Interfaces
{
    public interface IWorkSheetService
    {
        Task<ProccesResultDto> proccesDailyInfo(int doTime, int quantity, int paramNumber, int personNumber);

        Task<WorkSheet> CreateWorkSheetAsync(CreateDailyInformationViewModel viewModel);
    }
}