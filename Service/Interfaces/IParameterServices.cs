﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Parameter;

namespace Service.Interfaces
{
    public interface IParameterServices
    {
        Task CreateAsync(CreateParameterViewModel viewModel);

        Task<List<ParameterViewModel>> GetAllParametersAsync();
    }
}