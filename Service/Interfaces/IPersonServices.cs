﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Person;

namespace Service.Interfaces
{
    public interface IPersonServices
    {
        Task AddPersonAsync(CreatePersonViewModel viewModel);

        Task<List<PersonDetailViewModel>> GetAllPersonsAsync();
    }
}