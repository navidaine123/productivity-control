﻿using AutoMapper;
using Model;
using System;
using System.Collections.Generic;
using System.Text;
using ViewModel.Parameter;
using ViewModel.Person;

namespace Service.Shared
{
    public class MapperProFile : Profile
    {
        public MapperProFile()
        {
            CreateMap<CreatePersonViewModel, Person>();
            CreateMap<CreateParameterViewModel, Parameter>();
            CreateMap<ParameterViewModel, Parameter>().ReverseMap();
        }
    }
}