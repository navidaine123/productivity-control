﻿using AutoMapper;
using Model;
using Repository.Interfaces;
using Service.Interfaces;
using Service.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel.Person;

namespace Service
{
    public class PersonServices : IPersonServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPersonRepository _personRepository;

        public PersonServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _personRepository = unitOfWork.PersonRepository;
        }

        public async Task AddPersonAsync(CreatePersonViewModel viewModel)
        {
            var person = new Person();
            _mapper.Map(viewModel, person);

            try
            {
                var lastPersonNumber =
                (await _personRepository.GetAllAsync())
                .Select(x => x.PersonNumber)
                .Max();

                person.PersonNumber = lastPersonNumber != null ? GeneratePersonNumber(lastPersonNumber) : "1";
            }
            catch (Exception e)
            {
                throw;
            }

            var res = await _personRepository.AddAsync(person);
            await _unitOfWork.SaveAsync();
        }

        public async Task<List<PersonDetailViewModel>> GetAllPersonsAsync()
        {
            var persons = await _personRepository.GetAllAsync();

            var res = _mapper.Map<List<PersonDetailViewModel>>(persons);

            return res;
        }

        private string GeneratePersonNumber(string lastnumber)
        {
            var value = Convert.ToInt32(lastnumber) + 1;
            return value.ToString();
        }
    }
}