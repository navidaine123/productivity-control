﻿using AutoMapper;
using Model;
using Repository.Interfaces;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Parameter;

namespace Service
{
    public class ParameterServices : IParameterServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IParameterRepository _parameterRepository;

        public ParameterServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _parameterRepository = unitOfWork.ParameterRepository;
        }

        public async Task CreateAsync(CreateParameterViewModel viewModel)
        {
            var parameter = new Parameter();
            _mapper.Map(viewModel, parameter);

            try
            {
                var lastParameterNumber =
                (await _parameterRepository.GetAllAsync())
                .Select(x => x.ParameterNumber)
                .LastOrDefault();

                parameter.ParameterNumber = lastParameterNumber != null ? GenerateParameterNumber(lastParameterNumber) : "1";
            }
            catch (Exception e)
            {
                throw;
            }

            var res = await _parameterRepository.AddAsync(parameter);
            await _unitOfWork.SaveAsync();
        }

        public async Task<List<ParameterViewModel>> GetAllParametersAsync()
        {
            var parameters = await _parameterRepository.GetAllAsync();

            var res = _mapper.Map<List<ParameterViewModel>>(parameters);

            return res;
        }

        private string GenerateParameterNumber(string lastnumber)
        {
            var value = Convert.ToInt32(lastnumber) + 1;
            return value.ToString();
        }
    }
}