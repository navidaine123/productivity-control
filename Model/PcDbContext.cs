﻿using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class PcDbContext : DbContext
    {
        public PcDbContext(DbContextOptions<PcDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=ProductiveControl;Trusted_Connection=True;");
        }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Parameter> Parameters { get; set; }

        public DbSet<WorkSheet> WorkSheets { get; set; }

        public DbSet<GeneralSettings> GeneralSettings { get; set; }
    }
}