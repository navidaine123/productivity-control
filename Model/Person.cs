﻿using Model.Enums;
using System;

namespace Model
{
    public class Person
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get { return FirstName + " " + LastName; } }

        public string PersonNumber { get; set; }

        public int BaseSalary { get; set; }

        public int BaseMinuteSalary { get; set; }

        public DeployType DeployType { get; set; }

        public string Position { get; set; }

        public DateTime DeployDate { get; set; }
    }
}