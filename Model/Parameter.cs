﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Parameter
    {
        public Guid Id { get; set; }

        public string ParameterNumber { get; set; }

        public string ParameterName { get; set; }

        public int StandardTime { get; set; }

        public int StandardCount { get; set; }

        public int WastedTime { get; set; }
    }
}