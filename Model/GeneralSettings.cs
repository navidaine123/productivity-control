﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class GeneralSettings
    {
        public Guid Id { get; set; }

        public string CompanyName { get; set; }

        public string EconomicId { get; set; }

        public string PostId { get; set; }

        public string Address { get; set; }

        public string Tell { get; set; }
    }
}