﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Enums
{
    public enum DeployType : byte
    {
        Daily = 1,
        Contract = 2
    }
}