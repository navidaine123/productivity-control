﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class WorkSheet
    {
        public Guid Id { get; set; }

        public Guid PersonId { get; set; }

        public Guid ParameterId { get; set; }

        public DateTime Date { get; set; }

        public int DoTime { get; set; }

        public int Quantity { get; set; }

        public int DiffrenceDoTime { get; set; }

        public int DiffrenceQuantity { get; set; }
    }
}