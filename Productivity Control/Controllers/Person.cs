﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using ViewModel.Person;

namespace Productivity_Control.Controllers
{
    public class Person : Controller
    {
        private readonly IPersonServices _personServices;

        public Person(IPersonServices personServices)
        {
            _personServices = personServices;
        }

        public async Task<IActionResult> Index()
        {
            var res = await _personServices.GetAllPersonsAsync();
            return View(res);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new CreatePersonViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreatePersonViewModel createPersonViewModel)
        {
            if (ModelState.IsValid)
            {
                await _personServices.AddPersonAsync(createPersonViewModel);
                return RedirectToAction("Index");
            }

            return View(createPersonViewModel);
        }
    }
}