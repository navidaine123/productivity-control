﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using ViewModel.Parameter;

namespace Productivity_Control.Controllers
{
    public class Parameter : Controller
    {
        private readonly IParameterServices _parameterServices;

        public Parameter(IParameterServices parameterServices)
        {
            _parameterServices = parameterServices;
        }

        [HttpGet]
        public async Task<IActionResult> Index() =>
            View(await _parameterServices.GetAllParametersAsync());

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateParameterViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                await _parameterServices.CreateAsync(viewModel);

                return RedirectToAction("index");
            }
            return View(viewModel);
        }
    }
}