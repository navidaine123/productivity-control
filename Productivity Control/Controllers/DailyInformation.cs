﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.VisualBasic;
using Service;
using Service.Interfaces;
using ViewModel.DailyInformation;

namespace Productivity_Control.Controllers
{
    public class DailyInformation : Controller
    {
        private readonly IPersonServices _personServices;
        private readonly IParameterServices _parameterServices;
        private readonly IWorkSheetService _workSheetService;

        public DailyInformation(IPersonServices personServices, IParameterServices parameterServices, IWorkSheetService workSheetService)
        {
            _personServices = personServices;
            _parameterServices = parameterServices;
            _workSheetService = workSheetService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> CreateAsync()
        {
            var persons = await _personServices.GetAllPersonsAsync();
            var parameters = await _parameterServices.GetAllParametersAsync();
            var paramItems = new Collection<ParameterSelectList>();

            foreach (var item in parameters)
            {
                paramItems.Add(new ParameterSelectList
                {
                    Text = item.ParameterName,
                    Value = item.ParameterNumber + "/" + item.StandardTime
                });
            }

            ViewBag.Persons = new SelectList(persons, "PersonNumber", "FullName");
            ViewBag.Parameters = new SelectList(paramItems, "Value", "Text");
            var model = new CreateDailyInformationViewModel
            {
                Date = DateTime.UtcNow,
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateDailyInformationViewModel viewModel)
        {
            var a = await _workSheetService.CreateWorkSheetAsync(viewModel);
            return RedirectToAction("Print", viewModel);
        }

        public IActionResult Print(CreateDailyInformationViewModel viewModel)
        {
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> ProccessResult(int dotime, int quantity, int parameterNumber, int personNumber)
        {
            var a = await _workSheetService.proccesDailyInfo(dotime, quantity, parameterNumber, personNumber);
            return Json(a);
        }
    }
}