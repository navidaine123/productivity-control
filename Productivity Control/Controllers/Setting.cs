﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ViewModel.Settings;

namespace Productivity_Control.Controllers
{
    public class Setting : Controller
    {
        [HttpGet]
        public IActionResult General()
        {
            return View();
        }

        [HttpPost]
        public IActionResult General(GeneralSettingsViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
            }
            return View();
        }

        public IActionResult Personal()
        {
            return View();
        }

        public IActionResult Parmameter()
        {
            return View();
        }
    }
}