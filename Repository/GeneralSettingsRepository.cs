﻿using Model;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class GeneralSettingsRepository : GenericRepository<GeneralSettings>, IGeneralSettingsRepository
    {
        private readonly PcDbContext _pcDbContext;

        public GeneralSettingsRepository(PcDbContext pcDbContext)
            : base(pcDbContext)
        {
            _pcDbContext = pcDbContext;
        }
    }
}