﻿using Model;
using Repositories;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IParameterRepository : IGenericRepository<Parameter>
    {
        Task<Parameter> GetByParameterNumber(string parameterNumber);
    }
}