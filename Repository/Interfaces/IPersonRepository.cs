﻿using Model;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IPersonRepository : IGenericRepository<Person>
    {
        Task<Person> GetPersonByNumber(string number);
    }
}