﻿using Model;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interfaces
{
    public interface IWorkSheetRepository : IGenericRepository<WorkSheet>
    {
    }
}