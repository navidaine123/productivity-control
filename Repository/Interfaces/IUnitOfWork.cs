﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IUnitOfWork
    {
        public IPersonRepository PersonRepository { get; }

        public IParameterRepository ParameterRepository { get; }

        public IWorkSheetRepository WorkSheetRepository { get; }

        void Save();
        Task SaveAsync();
    }
}
