﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {

        Task<T> AddAsync(T t);

        Task<T> GetAsync(object key);

        Task<ICollection<T>> GetAllAsync();

        
    }
}
