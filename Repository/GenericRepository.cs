﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly PcDbContext _pcDbContext;

        private DbSet<T> _entities;

        public GenericRepository(PcDbContext pcDbContext)
        {
            _pcDbContext = pcDbContext;
            _entities = _pcDbContext.Set<T>();
        }

        public async Task<T> AddAsync(T t)
        {
            await _entities.AddAsync(t);
            return t;
        }

        public virtual async Task<ICollection<T>> GetAllAsync()
        {
            try
            {
                var a = await _entities.ToListAsync();
                return a;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public Task<T> GetAsync(object key)
        {
            throw new NotImplementedException();
        }

        //TODO
    }
}