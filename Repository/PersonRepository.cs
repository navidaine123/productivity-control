﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class PersonRepository : GenericRepository<Person>, IPersonRepository
    {
        private readonly PcDbContext _pcDbContext;

        public PersonRepository(PcDbContext pcDbContext)
            : base(pcDbContext)
        {
            _pcDbContext = pcDbContext;
        }

        public async Task<Person> GetPersonByNumber(string number) =>
            await _pcDbContext.Persons
            .FirstOrDefaultAsync(x => x.PersonNumber.Equals(number));
    }
}