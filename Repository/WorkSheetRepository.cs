﻿using Model;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class WorkSheetRepository : GenericRepository<WorkSheet>, IWorkSheetRepository
    {
        private readonly PcDbContext _pcDbContext;

        public WorkSheetRepository(PcDbContext pcDbContext)
            : base(pcDbContext)
        {
        }
    }
}