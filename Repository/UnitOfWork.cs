﻿using Model;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PcDbContext _pcDbContext;

        public UnitOfWork(PcDbContext pcDbContext)
        {
            _pcDbContext = pcDbContext;
        }

        private IPersonRepository _personRepository;

        public IPersonRepository PersonRepository => _personRepository ??= new PersonRepository(_pcDbContext);

        private IWorkSheetRepository _workSheetRepository;

        public IWorkSheetRepository WorkSheetRepository => _workSheetRepository ??= new WorkSheetRepository(_pcDbContext);

        private IParameterRepository _parameterRepository;

        public IParameterRepository ParameterRepository => _parameterRepository ??= new ParameterRepository(_pcDbContext);

        public void Save()
        {
            _pcDbContext.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _pcDbContext.SaveChangesAsync();
        }
    }
}