﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ParameterRepository : GenericRepository<Parameter>, IParameterRepository
    {
        private readonly PcDbContext _pcDbContext;

        public ParameterRepository(PcDbContext pcDbContext)
            : base(pcDbContext)
        {
            _pcDbContext = pcDbContext;
        }

        public async Task<Parameter> GetByParameterNumber(string parameterNumber) =>
            await _pcDbContext.Parameters
            .FirstOrDefaultAsync(x => x.ParameterNumber == parameterNumber);
    }
}